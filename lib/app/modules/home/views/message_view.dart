import 'package:demo_notification/app/data/models/message.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MessageView extends StatelessWidget {
  const MessageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var message = Get.arguments as MessageArguments ;
    return Scaffold(
      appBar: AppBar(title: Text('Message View'),),
      body: Container(
        child: Center(child: Text('${message.message.data}'),),
      ),
    );
  }
}
