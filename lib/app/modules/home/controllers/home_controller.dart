import 'package:demo_notification/app/data/models/message.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  var currentMSG= {};

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  @override
  onInit() {
    super.onInit();
    _registerNotification();
  }

  Future<void> _registerNotification() async {
    await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    //Get token Auth <= token
    _firebaseMessaging.getToken().then((token) {
      print('TOKEN: $token');
    }).catchError((err) {
      print(err.message.toString());
    });
    //Subscribe to Topic
    _firebaseMessaging.subscribeToTopic('topicKhuyen');
    //Listen
    _firebaseMessaging.getInitialMessage().then((message) {
      print('MSG_Initial $message');
      if (message != null) {
        Get.toNamed('/message', arguments: MessageArguments(message, true));
      }
    });
    //Handle message
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      print('MSG: Receive onMessage ${message.data} ');
      if (notification != null && android != null) {
        print('MSG: ${message.data['title']} - ${message.data['body']}');
      } else {
        print(
            'MSG: ${message.notification!.title} - ${message.notification!.body}');
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('OPEN_APP: $message ');
    });
  }
}
