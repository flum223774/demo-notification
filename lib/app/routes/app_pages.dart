import 'package:demo_notification/app/modules/home/views/message_view.dart';
import 'package:get/get.dart';

import 'package:demo_notification/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
    ),
    GetPage(
      name: _Paths.MESSAGE,
      page: () => MessageView(),
    ),
  ];
}
